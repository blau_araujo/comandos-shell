# Lembretes de comandos do shell do GNU/Linux

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

## Links importantes

* [Vídeos e textos para aprender o shell do GNU/Linux](https://codeberg.org/blau_araujo/para-aprender-shell)
* [Dúvidas sobre os comandos (issues)](https://codeberg.org/blau_araujo/comandos-shell/issues)
* [Discussões sobre todo o nosso conteúdo (Gitter)](https://gitter.im/blau_araujo/community)

## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)

## Índice geral

[1 - Ações básicas com arquivos](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md)

- [Listar diretórios e arquivos](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#listar-diret%C3%B3rios-e-arquivos)
- [Em que diretório você está?](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#em-que-diret%C3%B3rio-voc%C3%AA-est%C3%A1)
- [Mudar de diretório](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#mudar-de-diret%C3%B3rio)
- [Criar um diretório](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#criar-um-diret%C3%B3rio)
- [Criar arquivos vazios](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#criar-arquivos-vazios)
- [Criar ligações simbólicas](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#criar-liga%C3%A7%C3%B5es-simb%C3%B3licas)
- [Remover arquivos e diretórios (cuidado!)](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#remover-arquivos-e-diret%C3%B3rios-cuidado)
- [Copiar arquivos e diretórios](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#copiar-arquivos-e-diret%C3%B3rios)
- [Mover ou renomear arquivos e diretórios](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#mover-ou-renomear-arquivos-e-diret%C3%B3rios)
- [Alterar permissões de arquivos](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#alterar-permiss%C3%B5es-de-arquivos)
- [Exibir o conteúdo de arquivos de texto](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#exibir-o-conte%C3%BAdo-de-arquivos-de-texto)
- [Enviar linhas de texto para arquivos](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#enviar-linhas-de-texto-para-arquivos)
- [Enviar linhas de texto para comandos](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#enviar-linhas-de-texto-para-comandos)
- [Editar arquivos de texto plano](https://codeberg.org/blau_araujo/comandos-shell/src/branch/main/tabelas/acoes-basicas-com-arquivos.md#editar-arquivos-de-texto-plano)
